import java.util.Random;
import java.util.Scanner;

public class myHumanguess {
    public static void main (String args[]) {
        System.out.println("Hey! The number between 1 and 100.");
        System.out.println("Figure it out");

        do {
            round();
        }
        while (playAnother());
    }

    public static void round() {
        Random rand = new Random();
        int compNum;
        int comp;
        int userIn;
        int round = 0;
	Scanner scan = new Scanner(System.in);

        compNum = rand.nextInt(100);
        comp = compNum+1;

        do {
        System.out.println("Your guess:");
        userIn = scan.nextInt();

        if (userIn > comp) {
                round++;
                System.out.println("Number was too high");
            }
            else if (userIn < comp) {
                round++;
                System.out.println("Number was too low");
            }
            else {
                round++;
                System.out.println("You took "+ round +" tries. My number was "+ comp + "You're no mindreader");
            }
        }
        while (!(userIn == comp));
    }

    public static boolean playAnother() {
        Scanner scann = new Scanner(System.in);

        System.out.println("Play Again? (y/n)");
        String yn = scann.next().toLowerCase();
        if (yn.equals("y")) {
            return true;
        }
        else if (yn.equals("n")) {
            return false;
        }
        else {
            System.out.println("y/n?.");
            return playAnother();
        }
    }
}
