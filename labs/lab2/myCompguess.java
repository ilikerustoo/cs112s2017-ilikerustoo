import java.util.Random;
import java.util.Scanner;

public class myCompguess {
    public static void main (String args[]) {
        System.out.println("Hey! Pick a number between 1 and 100.");
        System.out.println("If the number is too high then click 2");
        System.out.println("if the number is too low then click 1");
        System.out.println("if the number is correct then click 0");

        do {
            round();
        }
        while(playAnother());
    }

    public static void round() {
        double compNum = 50.0;
        int userGuess;
        int round = 0;
        double max = 0;
        double min = 0;
        Scanner scan = new Scanner(System.in);

        do {
            System.out.println("My guess is "+compNum+". How did I do?");
            userGuess = scan.nextInt();

            switch (userGuess) {
                case 0:
                    round++;
                    System.out.println("Yay, I got it in "+round+" tries! Your number was "+compNum);
                    break;

                case 1:
                    round++;
                    System.out.print("Hummm, my guess was too low.");
                    min = compNum;
                    compNum = Math.ceil(((max-min)/2)+min);
                    break;

                case -1:
                    round++;
                    System.out.print("Hummm, my guess was too high.");
                    max = compNum;
                    compNum = Math.ceil(((max-min)/2)+min);
                    break;

                default:
                    System.out.println("You have to input 0, 1 or -1.");
                    break;
            }
        }
        while (!(userGuess == 0));

    }

    public static boolean playAnother() {
        Scanner scann = new Scanner(System.in);

        System.out.println("Play another round? y/n");
        String yn = scann.next().toLowerCase();
        if (yn.equals("y")) {
            return true;
        }
        else if (yn.equals("n")) {
            return false;
        }
        else {
            System.out.println("y/n?");
            return playAnother();
        }
    }
}
