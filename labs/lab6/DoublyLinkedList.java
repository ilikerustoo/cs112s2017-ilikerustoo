class DoublyLinkedList {

	private static class Node {

		protected int data;
		protected Node next, previous;

		public Node() {
			next = null;
			data = 0;
			previous = null;
		} //Node (constructor)

		public Node(int d, Node n, Node p) {
			next = n;
			data = d;
			previous = p;
		} //Node (constructor)

	} //Node (class)



	// DoublyLinkedList stuff starts here
	private int size;
	private Node head, tail;

	public DoublyLinkedList() {
		head = null;
		tail = null;
		size = 0;
	} //DoublyLinkedList (constructor)

	public int size() {
		return size;
	} //size

	public boolean isEmpty() {
		return (size == 0);
	} //isEmpty

	public void add(int inty) {
    Node moes = new Node(inty, null, null);
		if(size== 0){
			head = moes;
			tail = moes;
			size = 1;
			return;

		}
                else{
		tail.next = moes;
		moes.previous = tail;
		tail = moes;
		size++;
                }

	} //adding

	public int get(int index) {
		Node currentPosition = head;
		int counter = 0;

		// check to make sure the index is less than size
		if (index >= size || index < 0) {
			System.out.println("Hey you gave me bad input -- get");
			return -1;
		} //if

		while (counter < index) {
			currentPosition = currentPosition.next;
			counter++;
		} //while

		return currentPosition.data;
	} //get

	public int getFromEnd(int index) {

		Node lastPos = tail;
		int getters = 0;

		if(index >= size || index < 0)
		{
			System.out.println("You gave me a bad input");
			return -1;
		}

		while(getters < index)
		{
			lastPos = lastPos.previous;
			getters++;
		}

		return lastPos.data;





	} //getFromEnd

	public void remove(int index) {

		if(index <= -1 || index >= size)
		{
			System.out.println("Hey you provided a bad input -- remove");
		}

		if(index == 0)
		{

			if(size == 1)
			{
				head = null;
				tail = null;
			}
			else{
				head = head.next;
				head.previous.next = null;
				head.previous = null;
			}
		}
		else
		{
                        if(index == size-1)
		     {
			tail = tail.previous;
			tail.next.previous = null;
			tail.next = null;
		     }
                     else{

			Node currentPosition = head;
			int counter = 0;
			while(counter < index)
			{       //System.out.println("counter = " +counter);
				 //System.out.println("index =" +index);
				currentPosition = currentPosition.next;
				counter++;
			}

			currentPosition.previous.next = currentPosition.next;
			currentPosition.next.previous = currentPosition.previous;
			currentPosition.next = null;
			currentPosition.previous = null;
		         }
		}
		size--;




	} //remove

	public boolean testPreviousLinks() {
		Node current = tail;
		int count = 1;
		while (current.previous != null) { //System.out.println(current.data);
			current = current.previous;
			count++;
		} //while
		return ((current == head) && (count == size));
	} //testPreviousLinks

} //END
