class DoublyLinkedListTester {

	public static void main(String args[]) {
		DoublyLinkedList moesList = new DoublyLinkedList();
		addItems(moesList, 4);
		printCurrentState(moesList);
		moesList.remove(2);
		printCurrentState(moesList);
		System.out.println(moesList.getFromEnd(2) + "\n");
		System.out.println(moesList.testPreviousLinks());
		removeAll(moesList);
		moesList.add(71);
		printCurrentState(moesList);
	} //This is the main function

	public static void addItems(DoublyLinkedList currentList, int number) {
		for (int i = 0; i < number; i++) {
			currentList.add(i+1);
		} //for
	} //if

	public static void printCurrentState(DoublyLinkedList currentList) {
		System.out.println("Printing list contents:");
		for (int i = 0; i < currentList.size(); i++) {
			System.out.println(currentList.get(i));
		} //for
		System.out.println();
	} //printCurrentState

	public static void removeAll(DoublyLinkedList currentList) {
		int s = currentList.size();
		System.out.println("\nEmptying list");
		for (int i = 0; i < s; i++) {
			currentList.remove(0);
		} //if
		System.out.println(currentList.isEmpty());
		System.out.println();
	} //removeAll

} //SinglyLinkedList (class)
