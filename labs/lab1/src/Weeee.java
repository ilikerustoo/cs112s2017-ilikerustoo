
//This is the class "Weee"
public class Weeee {

    //This means nothing is returned
    public static void weeee() {
        //Message "Weeee!" is printed
		System.out.println("Weeee!");
//  Method is being called
		weeee();
    } //weeee

    public static void main(String[] args) {
        //Method "Weeee" being called
		weeee();
    } //main

} //Weeee (class)
