
//This is the class named "Hooray"
public class Hooray {

    public static void hooray() {
        //This is a while loop that makes program print Hooray infinitely
		while(true) {
			System.out.println("Hooray!");
    	} //while

    } //hooray

    public static void main(String[] args) {
        //Method "Hooray" is being called
		hooray();
    } //main

} //Hooray (class)
