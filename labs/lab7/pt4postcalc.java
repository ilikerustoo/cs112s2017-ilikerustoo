import java.util.*;


public class pt4postcalc {
public static void main (String args[]) {

Scanner scan = new Scanner(System.in);
System.out.println("Input the postfix:");
String conversion = scan.nextLine();
Stack<Integer> stack = new Stack<Integer>();


for(int m = 0; m < conversion.length(); m++){

    int a = 0;
    int b = 0;
    int result = 0;
    if(Character.isDigit(conversion.charAt(m))) {
       int convertString = Character.getNumericValue(conversion.charAt(m));
        stack.push(convertString);
    }

    else if(conversion.charAt(m) == '+') {
      if(!(stack.isEmpty())) {
        a = stack.pop();
        b = stack.pop();
        result = a+b;
        stack.push(result);
      }
    }
     else if(conversion.charAt(m) == '-') {
        if(!(stack.isEmpty()))
        {
        a = stack.pop();
        b = stack.pop();
        result = a-b;
        stack.push(result);
      }
    }
     else if(conversion.charAt(m) == '*') {
        if(!(stack.isEmpty()))
        {
        a = stack.pop();
        b = stack.pop();
        result = a*b;
        stack.push(result);
      }
    }
     else {
       if(!(stack.isEmpty())) {
        a = stack.pop();
        b = stack.pop();
        result = a/b;
        stack.push(result);
      }
    }
}

int calculatedValue = stack.pop();
System.out.println(calculatedValue);
 }
}
