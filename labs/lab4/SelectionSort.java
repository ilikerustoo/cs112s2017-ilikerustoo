import java.util.Arrays;

public class SelectionSort {
    public static void main(String args[]) {
        double[] s = {8,3,4,2,1};
        int minLoc;


        for (int i = 0; i < s.length; i++) {
            minLoc = i;
            double temp = s[i];

            for (int j = i; j < s.length; j++) {

                if (s[j] < s[minLoc]) {
                    minLoc = j;
                }

            }

            s[i] = s [minLoc];
            s[minLoc] = temp;
            System.out.println(Arrays.toString(s));
        }
    }
}
