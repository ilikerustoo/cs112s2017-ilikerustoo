import java.util.Scanner;
import java.util.Arrays;

public class CaesarCipher {
    public static void main (String args[]) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter words you want to encrypt (No punctuation allowed)");
        String input = scan.next();
        char[] msg = input.toCharArray();

        System.out.println("The encrypted version of the message is "+encrypt(msg)+" The decrypted version of the message is "+decrypt(msg));
    }

    public static String encrypt(char[] msg) {
        int shift = 3;
        int warp = 23;

        for (int i = 0; i < msg.length; i++) {
            if ((msg[i] < 'X')&&(msg[i] > '@')) {
                msg[i] += shift;
            }

            else if ((msg[i] < 'x')&&(msg[i] > '`')) {
                msg[i] += shift;
            }

            else if (msg[i] == ' ') {
                msg[i] = msg[i];
            }

            else if (msg[i] > 'w') {
                msg[i] -= warp;
            }

            else if ((msg[i] > 'W')&&(msg[i] < '[')) {
                msg[i] -= warp;
            }
        }

        return new String(msg);
    }

    public static String decrypt(char[] msg) {
        int shift = 3;
        int warp = 23;

        for (int i = 0; i < msg.length; i++) {
            if ((msg[i] < 'D')&&(msg[i] > '@')) {
                msg[i] += warp;
            }

            else if ((msg[i] < 'd')&&(msg[i] > '`')) {
                msg[i] += warp;
            }


            else if (msg[i] == ' ') {
                msg[i] = msg[i];
            }

            else if ((msg[i] > 'C')&&(msg[i] < '[')) {
                msg[i] -= shift;
            }

            else if ((msg[i] > 'c')&&(msg[i] < '{')) {
                msg[i] -= shift;
            }

        }

        return new String(msg);
    }
}
