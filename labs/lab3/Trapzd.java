public class Trapzd extends Quad {
    private double otherBase;
    private double slide1;
    private double slide2;

    public Trapzd(double width, double height, double otherBase, double slide1, double slide2) {
        setWidth(width);
        setHeight(height);
        this.otherBase = otherBase;
        this.slide1 = slide1;
        this.slide2 = slide2;
    }

    public void calculateArea() {
        setArea(getHeight()*(getWidth()+otherBase)/2);
    }

    public void calculatePerimeter() {
        setPerimeter(getWidth()+otherBase+slide1+slide2);
    }
}
