public class Circle extends Round {
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public void calculateArea() {
        setArea(2*getPi()*radius);
    }

    public void calculatePerimeter() {
        setPerimeter(getPi()*Math.pow(radius, 2));
    }
}
