public class Square extends Rect
{
    public Square(double width) {
        super(width, width);
    }

    public void calculateArea() {
        setArea(Math.pow(getWidth(), 2));
    }

    public void calculatePerimeter() {
        setPerimeter(4*getWidth());
    }
}
