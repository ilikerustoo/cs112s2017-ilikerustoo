public class Rect extends Quad {
    public Rect(double width, double height){
        setWidth(width);
        setHeight(height);
    }

    public void calculateArea() {
        setArea(getWidth()*getHeight());
    }
    public void calculatePerimeter() {
        setPerimeter(2*(getWidth()+getHeight()));
    }
}
