public class Oval extends Round {
    private double major;
    private double minor;

    public Oval(double major, double minor) {
        this.major = major;
        this.minor = minor;
    }

    public void calculateArea() {
        setArea(getPi()*major*minor);
    }

    public void calculatePerimeter() {
        setPerimeter(2*getPi()*Math.sqrt((Math.pow(major, 2)+Math.pow(minor, 2))/2));
    }
}
