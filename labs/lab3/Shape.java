public class Shape {
    protected double area;
    protected double perimeter;


    public void setArea(double area) {
        this.area = area;
    }

    public void setPerimeter(double perimeter) {
        this.perimeter = perimeter;
    }
    // Areas and Parameters with child classes

    public double getArea() {
        return area;
    }

    public double getPerimeter() {
        return perimeter;
    }
    // Values from Child classes

}
