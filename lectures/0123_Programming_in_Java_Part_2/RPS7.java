import java.util.Scanner;
import java.util.Random;

public class RPS7 {

	public static void main(String[] args) {

		int humanPoints = 0, computerPoints = 0, tiePoints = 0;

		do {

			playRound(humanPoints, computerPoints, tiePoints);


		} while (playAgain()); // do-while

		System.out.println("Hey thanks for playing");

	} // main

	public static int roundWinner(String u, String c) {
		// 0 = user wins
		// 1 = computer wins
		// 2 = there was a tie

		if (u.equals(c)) {
			return 2;

		} else if (u.equals("ROCK") && c.equals("PAPER")) {
			return 1;

		} else if (u.equals("SCISSORS") && c.equals("ROCK")) {
			return 1;

		} else if (u.equals("PAPER") && c.equals("SCISSORS")) {
			return 1;

		} else {
			return 0;

		} //if-else
	} //roundWinner

	public static String generateComputerChoice() {
		// 0 = ROCK
		// 1 = PAPER
		// 2 = SCISSORS

		Random rand = new Random();
		int computerChoice;

		computerChoice = rand.nextInt(3);

		if (computerChoice == 0) {
			System.out.println("Computer picks ROCK");
			return "ROCK";
		} else if (computerChoice == 1) {
			System.out.println("Computer picks PAPER");
			return "PAPER";
		} else if (computerChoice == 2) {
			System.out.println("Computer picks SCISSORS");
			return "SCISSORS";
		} //if-else

		System.out.println("Something very bad has happened, returning ROCK by default");
		return "ROCK";
	} //generateComputerChoice

	public static String getUserInput() {
		Scanner scan = new Scanner(System.in);

		System.out.println("Enter your choice (ROCK, PAPER, SCISSORS): ");
		String userChoice;

		userChoice = scan.next().toUpperCase();

		return userChoice;
	} //getUserInput

	public static boolean playAgain() {


		return false;   // TODO: get rid of this in the final version
	} //playAgain

	public static void playRound(int userScore, int computerScore, int tiesScore) {
		String userChoice;
		String computerChoice;
		int winner;

		// Get a command from the user
		userChoice = getUserInput();

		// Generate a computer choice
		computerChoice = generateComputerChoice();

		// Pick a winner
		winner = roundWinner(userChoice, computerChoice);

		// Increment the appropriate score
		switch (winner) {

			case 0:
				userScore++;
				System.out.println("User is the winner.");
				break;

			case 1:
				computerScore++;
				System.out.println("Computer is the winner.");
				break;

			case 2:
				tiesScore++;
				System.out.println("The result is a tie");
				break;

			default:
				System.out.println("Something very bad has happened.");
				break;

		} //switch

		// Display the scores
		printScores(userScore, computerScore, tiesScore);


	} //playRound

	public static void printScores(int u, int c, int t) {

	} //printScores

} // RPS7 (class)