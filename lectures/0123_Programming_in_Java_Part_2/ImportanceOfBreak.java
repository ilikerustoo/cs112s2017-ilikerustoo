public class ImportanceOfBreak {

	public static void main(String[] args) {

		int var = 1;
		int x;

		if (var == 0) {
			x = 3;
		} else if (var == 1) {
			x = 5;
		} else if (var == 2) {
			x = 7;
		} else if (var == 3) {
			x = 12;
		} else {
			x = 0;
		} //if-else

		System.out.println("The output after the if-else is: " + x);

		switch (var) {
			case 0:
				x = 3;
			case 1:
				x = 5;
			case 2:
				x = 7;
			case 3:
				x = 12;
			default:
				x = 0;
		} //switch

		System.out.println("The output after the switch/case without breaks is: " + x);

		switch (var) {
			case 0:
				x = 3;
				break;
			case 1:
				x = 5;
				break;
			case 2:
				x = 7;
				break;
			case 3:
				x = 12;
				break;
			default:
				x = 0;
				break;
		} //switch

		System.out.println("The output after the switch/case with breaks is: " + x);

	} // main

} // ImportanceOfBreak (class)
